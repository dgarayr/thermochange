#!/bin/bash
#Diego Garay-Ruiz, Spring 2019. ADF output file parser for THERMOCHANGE.
#Input from STDIN: filename, temperature, pressure, correction type.
#Extracted information: frequency list, recalc. frequency list, bond energy, molecular weight,
#moments of inertia

bash_script_name="parser_adf"
program="ADF"
#Assignment of temperature and pressure

if [ -z $3 ] ; then
	echo "Enter filename, T/K and P/atm"
	echo "Using default temperature and pressure:"
	echo "T = 298.15 K, P = 1.0 atm"
	echo "----------------------------"
	temperature=298.15
	pressure=1.0
else
	temperature=$2
	pressure=$3
fi

#Filename assignment
filename=$1

#Consider the symmetry number
sigma=$(grep 'sigma = ' $1 | tail -n 1 | tr -d '[A-Z][a-z]=,')
#for monoatomics, there is no rotation and no symmetry number
#assign a flag
if [ -z ${sigma} ] ; then
	sigma=999
fi

#Request correction type
if [ -z $4 ]; then
	corrtype="RRHO"
else
	if [ $4 == "Grimme" ] ; then
			corrtype="Grimme"
	else
			corrtype="RRHO"
	fi
fi

#Script location
script_route=${BASH_SOURCE[0]}
script_folder=$(echo "${script_route}" | sed "s|/${bash_script_name}.sh||g" | sed "s|/parsers||g")
echo $script_folder

#Get the block of frequencies
sed '1,/List of All Frequencies/d;/Statistical*/,$d' $1 | tail -n+9 | head -n -4 | awk '{print $1}' > freq_info
#Consider recalculated (SCANFREQ) frequencies too
sed '1,/Old      New /d;/not re-calc/,$d' $1 | tail -n+3 | awk '{print $1,"   ",$2}' > recalc_freqs

# Bond energy: be careful when LOGFILE has not been supressed from output
# by adding another SED removing all <...> bearing entries which correspond to LOGFILE
bond_energy=$(grep 'Total Bonding Energy' $1 | sed "/<.*>/d" | tail -n 1 | awk '{print $4}')
echo $bond_energy > energy.txt #in a.u.

#Atomic masses
#explanation of the pipe: fetches list of atomic masses, removes spurious lines (----- and whitespaces)
#then fetches only the numeric values, pastes them in a single string of the form a+b+c+d... and feeds it to bc
#In order: get block of masses -> remove first line -> remove last line -> take only third field (mass) -> convert to single-line string separated by +
#paste -sd+: s -> serial, to convert to row-like, -d+ -> use + separator
mass=$(sed '1,/At\.Mass/d;/FRAGMENTS/,$d' $1 | tail -n +2 | sed '/^$/d' | awk '{print $8}' | paste -sd+ | bc)
echo $mass > mass.txt #in u

#Moments of inertia
sed '1,/Moments of Inertia*/d;/-----*/,$d' $1 | tail -n 1 > moments_inertia.txt

#Call Python script
#python3 ${script_folder}/thermochange.py ${program} ${temperature} ${pressure} ${sigma} ${corrtype}
python3 ${script_folder}/thermochange.py ${program} -T ${temperature} -P ${pressure} -s ${sigma} --vibr_entropy ${corrtype}

#cleanup
# rm freq_info
# rm energy.txt
# rm mass.txt
# rm moments_inertia.txt
