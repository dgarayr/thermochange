#!/bin/bash
#Diego Garay-Ruiz, Spring 2019. ADF output file parser for THERMOCHANGE.
#Input from STDIN: filename, temperature, pressure, symmetry number, correction type.
#Extracted information: frequency list, energy, molecular weight, rotational temperatures

bash_script_name="parser_g09"
program="G09"
#Assignment of temperature and pressure
if [ -z $3 ] ; then
	echo "Enter filename, T/K and P/atm (and symmetry number)"
	echo "Using default temperature and pressure:"
	echo "T = 298.15 K, P = 1.0 atm"
	echo "----------------------------"
	temperature=298.15
	pressure=1.0
else
	temperature=$2
	pressure=$3
fi

#Filename assignment
filename=$1

#Symmetry number
sigma=$(grep 'symmetry number' ${filename} | tr -d '[A-Z][a-z]' | sed "s/\.//g")
#for monoatomics, there is no rotation and no symmetry number
#assign a flag
if [ -z ${sigma} ] ; then
	sigma=999
fi
#Request correction type
if [ -z $4 ]; then
	corrtype="RRHO"
else
	if [ $4 == "Grimme" ] ; then
			corrtype="Grimme"
	else
			corrtype="RRHO"
	fi
fi
#Script location
script_route=${BASH_SOURCE[0]}
script_folder=$(echo "${script_route}" | sed "s|/${bash_script_name}.sh||g" | sed "s|/parsers||g")

#Get frequencies
sed -n "/Frequencies --/p" ${filename} | sed "s/Frequencies --//g" | xargs | sed "s/ /\n/g" > freq_info
#pipeline explanation: print only lines that contain frequencies (x3 each), remove the trailing string, put in only one line
#and replace whitespaces by newlines to have a column

#No recalculation of frequencies exists in Gaussian

#Energy in a.u.
grep "SCF Done" ${filename} | tail -n 1 | sed "s/^.*=//g" | sed "s/A.U.*$//gI" > energy.txt
# override for MP2 calculations, where SCF Done shows the HF energy, not the corrected value!
mp2_flag=$(grep "EUMP2" ${filename} | tail -n1)
if [ ! -z "${mp2_flag}" ] ; then
	  # extract the energy: overwrite
	  e=$(echo $mp2_flag | awk '{gsub(/^.*EUMP2 =/,"");gsub(/D/,"E")} ; {printf "%20.9f\n",$0}')
    echo "MP2 calculation found: overriding SCF energy"
    echo $e > energy.txt
fi

#Molecular mass
grep "Molecular mass:" ${filename} | awk '{print $3}' > mass.txt

#Rotational temperatures 
grep "Rotational temperature" ${filename} | sed "s/^.*(Kelvin)//g" | xargs | sed "s/ /\n/g" > rot_temps.txt

#Call Python script
echo ${program} ${temperature} ${pressure} ${sigma} ${corrtype}
#python3 ${script_folder}/thermochange.py ${program} ${temperature} ${pressure} ${sigma} ${corrtype}
python3 ${script_folder}/thermochange.py ${program} -T ${temperature} -P ${pressure} -s ${sigma} --vibr_entropy ${corrtype}

#Cleanup
#rm freq_info
#rm energy.txt
#rm mass.txt
#rm rot_temps.txt
