'''2019 - 2022, Diego Garay-Ruiz
THERMOCHANGE: Python library to compute thermodynamical magnitudes
at requested values of pressure and temperature for both ADF and Gaussian'''
import numpy as np
import sys
import scipy.constants as spc
from scipy.constants import h as h_joule
from scipy.constants import k as kb
from scipy.constants import c, physical_constants
from scipy.constants import R as R_SI
from scipy.constants import Avogadro as Navog
import scipy.constants
import argparse

# Unit converstions and constant specifications
h_wavenum = spc.h*spc.c*100 # to multiply by cm-1 wavenumbers, is in J · cm
R_kcal = spc.R/(4.184*1000) #in kcal/(mol K)
amu_to_kg = spc.physical_constants['atomic mass constant'][0]
bohr_to_m = spc.physical_constants['atomic unit of length'][0]
ang_to_bohr = 1/bohr_to_m*1E-10
hartree_to_J = spc.physical_constants['atomic unit of energy'][0]
R_hartree = spc.R/(hartree_to_J*spc.Avogadro) # in hartree/K
eV_to_J = spc.physical_constants['atomic unit of charge'][0]
kcalmol_to_hartree = 4184/(hartree_to_J*spc.Avogadro)

# ====Definition of vibr. term calculation functions====
def Evibr_RRHO(freqlist,temp,program):
	'''
	Compute vibrational contribution to thermal energy, in Joule.
	Input: 
	- freqlist. List of floats: frequencies in cm-1.
	- temp. Float, temperature in K.
	- program. String, stating the program: ADF or G09.
	Output:
	- E_vibr. Float, vibrational energy.
	'''
	E_vib = 0.0
	freqlimit = 0
	#ADF ignores frequencies under 20 cm-1: take this into account.
	if (program == "ADF"):
		freqlimit = 20

	# Go along frequencies: recall that ZPE correction is already included, as vibrational partition
	# function is taken from the bottom of the well
	for freq in freqlist:
		if (freq < freqlimit):
			continue
		theta_v = h_wavenum*freq/kb
		term1 = theta_v/(2*temp)
		term2 = (theta_v/temp)/(np.exp(theta_v/temp)-1)
		term = term1 + term2
		E_vib = E_vib + term
	return E_vib

def Svibr_RRHO(freqlist,temp,program,print_now=False):
	'''
	Compute vibrational entropy with RRHO traditional expression
	(rigid rotor/harmonic oscillator)
	
	Physical constants require scipy.constants and numpy
	Input: 
	- freqlist. List of floats: frequencies in cm-1.
	- temp. Float, temperature in K.
	- program. String, stating the program: ADF or G09.
	Output:
	- S_vibr. Float, vibrational entropy from RRHO approx. 
	'''
	S_vibr = 0
	skipped_freqs = []
	freqlimit = 0
	# Consider cutoff of small frequencies in ADF
	if (program == "ADF"):
		freqlimit = 20

	for freq in freqlist:
		if (freq < freqlimit):
			skipped_freqs.append(freq)
			continue
		
		theta_v = h_wavenum*freq/spc.k
		term1 = (theta_v/temp)/(np.exp(theta_v/temp)-1)
		term2 = np.log(1 - np.exp(-theta_v/temp))
		S_vibr_term = term1 - term2
		S_vibr += S_vibr_term
		
	if (print_now):
		print("Skipped",len(skipped_freqs),"small or negative frequencies")
		[print("%.2f cm-1" % neg_freq) for neg_freq in skipped_freqs]
	return S_vibr

def Svibr_Grimme(freqlist,temp,program,f_cutoff=100,no_interp=False,print_now=False):
	'''
	Compute vibrational entropy with pseudorrotational approach for small
	frequencies: below the cutoff, normal modes are also treated as rotations
	and the entropy contribution is interpolated between rot. and vibr. expressions
	
	Physical constants require scipy.constants and numpy
	Input: 
	- freqlist. List of floats: frequencies in cm-1.
	- temp. Float, temperature in K.
	- program. String, stating the program: ADF or G09.
	- f_cutoff. Float, cutoff frequency in cm-1. Default value of 100 cm-1 comes from
	Grimme Chem. Eur. J. 2012
	- no_interp. Boolean, if True do not interpolate RRHO vibration but only consider
	pseudorrotational approach
	Output:
	- S_vibr. Float, vibrational entropy from Grimme approximation.
	'''

	print("Adding RRHO interpolation",not no_interp)
	print("Freq. cutoff = %.2f cm-1" % f_cutoff)
	# Initialization & setup of parameters from Grimme Chem. Eur. J. 2012
	S_vibr = 0
	skipped_freqs = []
	freqlimit = 0
	f_cutoff = f_cutoff     			    #in cm-1, from Grimme Chem. Eur. J. 2012
	Bavg = 10E-44                        #in kg·m2, from Grimme Chem. Eur. J. 2012
	# Consider cutoff of small frequencies in ADF
	if (program == "ADF"):
		freqlimit = 20
	for freq in freqlist:
		if (freq < freqlimit):
			skipped_freqs.append(freq)
			continue
		#Both RRHO and pseudorrotational contributions must be computed, then weighted

		#Pseudorrotational approach (Grimme Chem. Eur. J. 2012):
		freq_hz = freq*100*spc.c
		m_inertia = spc.h/(8*np.pi**2*freq_hz)
		m_inertia_eff = m_inertia*Bavg/(m_inertia+Bavg)
		logterm = 8*np.pi**3*m_inertia_eff*spc.k*temp/spc.h**2
		s_vibr_pseudorrot = 0.5*(1 + np.log(logterm))

		#RRHO approximation
		theta_v = h_wavenum*freq/spc.k
		term1 = (theta_v/temp)/(np.exp(theta_v/temp)-1)
		term2 = np.log(1-np.exp(-theta_v/temp))
		s_vibr_rrho = term1 - term2

		#Weight according to the quotient against the cutoff freq. 100 cm-1: if no interpolation, weight is zero
		# so only pseudorrotation is taken into account 
		weight = 1/(1+(f_cutoff/freq)**4)
		if no_interp:
			weight = 0
		S_vibr_term = weight*s_vibr_rrho + (1-weight)*s_vibr_pseudorrot
		S_vibr += S_vibr_term
	if (print_now):
		print("Skipped",len(skipped_freqs),"small or negative frequencies")
		[print("%.2f cm-1" % neg_freq) for neg_freq in skipped_freqs]
	return S_vibr

#====Input management: check that all variables are provided====
class GibbsHandler:
	def __init__(self):
		# Default values for temperature, pressure and symmetry number (and corr. type)
		# are provided here
		self.temp = 298.15
		self.Patm = 1.0
		self.sigma = 1
		self.S_corr_type = "RRHO"
		# Placeholders for information from files
		self.freqlist = []
		self.energy = 0.0
		self.mass_kg = 0.0
		# Both rotational temperatures (Gaussian) or moments of inertia (ADF) can be provided here: allow both
		self.rot_temps = []
		self.moments_inertia = []

		### Results: energies in E/RT units
		self.ThermalEnergy = 0.0 
		self.TotalEnergy = 0.0
		self.Entropy = 0.0
		self.FreeEnergy = 0.0
		
		# Parser filenames used as default
		self.ParserDefaultNames = {'frequency':'freq_info','energy':'energy.txt',
								   'mass':'mass.txt'}

		# Unit conversions?
		# Printing level: 0 for nothing, 1 for Eel and G, 2 for complete output, 3 for complete output + individual components
		self.PrintLevel = 2

		# Parameters for Grimme correction
		self.FreqCutoff = 100			# in cm-1, default from paper
		self.CancelInterpolateRRHO = False		# Do not weight pseudorrotational approach with the RRHO contribution
		
		
	def set_program(self,program_name):
		# Allow to set program-specific aspects without relying on command_line_IO()
		self.program_name = program_name
		if (program_name == "G09"):
			self.ParserDefaultNames['rotational'] = 'rot_temps.txt'
		elif (program_name == "ADF"):
			self.ParserDefaultNames['rotational'] = 'moments_inertia.txt'
			self.ParserDefaultNames['recalc_frequency'] = 'recalc_freqs'
		elif (program_name == "XTB"):
			self.ParserDefaultNames['rotational'] = 'moments_inertia.txt'
			
		return None

	def cmd_IO(self):
		args = argparse.ArgumentParser()
		args.add_argument("program_name",help="Program the calculation was done with",type=str)
		args.add_argument("--temp","-T",help="Temperature in K, default 298.15 K",default=298.15,type=float)
		args.add_argument("--pressure","-P",help="Pressure in atm, default 1.0 atm",default=1.0,type=float)
		args.add_argument("--sigma","-s",help="Rotation number, default 1",default=1,type=int)
		args.add_argument("--vibr_entropy","-vS",help="Type of vibr. entropy, RRHO (default) or Grimme",
						  default="RRHO",type=str)
		args.add_argument("--print_level","-pl",help="Verbosity of the output, 0, 1 or 2",type=int,default=3)
		args.add_argument("--no-interp","-ni",help="For Grimme vibr. treatment, do not add weighted RRHO contribution",
						  action="store_true")
		args.add_argument("--freq-cutoff","-fc",help="For Grimme vibr. treatment, select vibration cutoff in cm-1",
						  default=100,type=float)
		try:
			argset = args.parse_args()
		except:
			args.print_help()
			sys.exit()

		# And use the parameters to fill the object
		self.temp = argset.temp
		self.Patm = argset.pressure
		self.sigma = argset.sigma
		self.S_corr_type = argset.vibr_entropy
		self.set_program(argset.program_name)
		self.PrintLevel = argset.print_level

		self.FreqCutoff = argset.freq_cutoff
		self.CancelInterpolateRRHO = argset.no_interp
		return self
	
	def command_line_IO(self):
		#Process information given through command line, via sys.argv
		self.program_name = sys.argv[1]
		if len(sys.argv) < 4:
			print("Using defaults: 298.15 K, 1 atm")
		else:
			self.temp = float(sys.argv[2])
			self.Patm = float(sys.argv[3])
			self.sigma = int(sys.argv[4])
			self.S_corr_type = str(sys.argv[5])
		self.set_program(self.program_name)
		return None

	def set_parameters(self,input_dict):
		# Set parameters in the GibbsHandler from a properly key-ed dictionary
		for k,v in input_dict.items():
			if (getattr(self,k,None) != None):
				setattr(self,k,v)
		return self
	
	#==== File processing: read info from parsing script========#
	def frequency_reading(self,from_parser=True,in_freq_list=[],recalclist=[]):
		'''Read frequency information and correct it for ADF).
		from_parser flag states whether info is taken from default bash
		parsers or communicated in a different way'''
		if (self.program_name == "ADF" and from_parser):
			with open(self.ParserDefaultNames['recalc_frequency']) as filerecalc:
				recalc_dump = [entry.split() for entry in filerecalc.readlines()]
				recalclist = [(float(item[0]),float(item[1])) for item in recalc_dump]

		if (from_parser == False):
			freqlist = [float(frq) for frq in in_freq_list]
		else:
			freqlist = []
			with open(self.ParserDefaultNames['frequency']) as filefreqs:
				lines = filefreqs.read()
				for item in lines.split():
					#Adjust precision
					num_item = float(item)
					freqlist.append(round(num_item,3))
		# Process recomputed frequencies, as from ADF SCANFREQ keyword
		if (recalclist):
			original_frequencies = freqlist
			counter = 0
			for pair in recalclist:
				if pair[0] in freqlist:
					index_match = freqlist.index(pair[0])
					print("Substituting freq.",pair[0],"by",pair[1])
					freqlist[index_match] = pair[1]
					counter = counter+1
			if (counter != len(recalclist)):
				print("Some modified frequencies were not taken into account")

		# Clear rotational/translational modes with zero or almost-zero frequency (0.5 cm-1 example)
		freqlist = [freq for freq in freqlist if abs(freq) > 0.5]
		# Finalize by sorting the list
		self.freqlist = sorted(freqlist)
		
		return None

	def energy_reading(self,from_parser=True,in_energy_val=0.0):
		'''Read parsed energy in a.u. and convert to E/R (in units of temperature)
		from_parser argument allows for alternative input'''
		if (from_parser == False):
			energy_au = float(in_energy_val)
		else:
			with open(self.ParserDefaultNames['energy']) as fileenerg:
				energy_au = float(fileenerg.read())
		self.energy = energy_au/R_hartree
		return None

	def rotation_reading(self,from_parser=True,in_rot_temps=[],in_moi=[]):
		'''Rotational magnitudes: G09 provides rot. temps and ADF, moments of inertia.
		from_parser allows for alternative input: to simplify, only rotational temps are allowed'''

		if (from_parser == False):
			# Check what is being passed: moments of inertia OR rotational temperatures
			if (in_rot_temps):
				rot_temps = [float(theta) for theta in in_rot_temps]
				self.rot_temps = rot_temps
			elif (in_moi):
				self.moments_inertia = [float(moi) for moi in in_moi]

		else:
			# Depending on the program, use one or other parameter (rot. temp or moment of inertia)
			with open(self.ParserDefaultNames['rotational']) as fileinert:
				lines = fileinert.read()
			if (self.program_name == "G09"):
				# Rotational temperatures in Kelvin
				self.rot_temps = [float(item) for item in lines.split()]
			elif (self.program_name == "ADF" or self.program_name == "XTB"):
				# Moments of inertia in amu·bohr²
				self.moments_inertia = [float(item) for item in lines.split()]


		# XTB uses u·A**2 units for the moi: transform ang to bohr to have MOI in atomic units
		if (self.program_name == "XTB" and self.moments_inertia):
			moi_bohr = [Ival*ang_to_bohr**2 for Ival in self.moments_inertia]
			self.moments_inertia = moi_bohr
			
		# If moments of inertia are specified, convert to rotational temperatures
		# both for default ADF and for custom cases passing MOI in atomic units u·A
		# also, handle linear cases where there will be null entries
		
		if (self.moments_inertia and not self.rot_temps):
			moi_thr = 1E-04			# near-zero, using atomic units
			null_moi = [moi < moi_thr for moi in self.moments_inertia]
			# if some MOI is null, we will be in a linear case with a single rotor
			if any(null_moi):
				valid_moi = [moi for moi in self.moments_inertia if moi > moi_thr]
				moi_check = (valid_moi[0] - valid_moi[1]) > moi_thr
				if (moi_check):
					print("MOI values for linear molecule do not match")
				self.moments_inertia = [valid_moi[0]]	
				
			for Ivalue in self.moments_inertia:
				#Transform to kg m² and get rot. temperatures in K
				Ivalue_SI = Ivalue*amu_to_kg*(bohr_to_m)**2
				theta = h_joule**2/(8*(np.pi)**2*kb*Ivalue_SI)
				self.rot_temps.append(theta)
				
		return None

	def mass_reading(self,from_parser=True,in_mass=0.0):
		'''Mass: read in amu, pass to kg'''
		if (from_parser == False):
			mass = float(in_mass)
		else:
			with open(self.ParserDefaultNames['mass']) as filemass:
				mass = float(filemass.read())
		self.mass_kg = mass*amu_to_kg
		return None

	def default_parser_reading(self):
		# Read all information from the parsed data
		self.frequency_reading()     
		self.energy_reading()
		self.rotation_reading()
		self.mass_reading()
		return None

	### Calculation functions
	def compute_thermal_energy(self):
		'''Compute thermal energy correction for an initialized GibbsHandler.
		Output:
		- E_thermal. Float, thermal energy correction in terms of E/RT
		- E_total. Float, total energy (elec. + thermal) in terms of E/RT'''

		# ====Energy (E/NkT) calculation====
		# Unpack temperature, used in almost all expressions
		temp = self.temp
		# Electronic/bonding energy. Stored in units of temperature, scale by dividing by T
		E_elec = self.energy/temp
		self.Eelec = E_elec

		# Translational and rotational energies: consider 3/2 RT expression
		# (equipartition theorem: 1/2 RT for every degree of freedom)
		E_transl = 1.5
		E_rot = 1.5

		# This does not hold for linear molecules, lacking one rot. deg. of freedom
		# so E_rot = 2 * (1/2)RT = RT
		# Check no. of rotational temperatures to correct the energy
		Nrotors = len(self.rot_temps)
		
		if (Nrotors == 1):
			E_rot = 1.0

		# For monoatomics there is no rotation: this is caught with a flag for the symmetry number
		if (self.sigma == 999 or self.sigma == None):
			E_rot = 0.0

		# Vibrational energy: sum through all terms
		E_vib = Evibr_RRHO(self.freqlist,temp,self.program_name)

		#Sum & output printing: scale electronic energy
		E_thermal = E_transl + E_rot + E_vib
		E_terms = [E_transl,E_rot,E_vib]
		self.ThermalEnergy = E_thermal
		self.ThermalComponents = E_terms
		E_total = E_thermal + E_elec
		self.TotalEnergy = E_total
		# And also compute H = E + PV = E + RT. As we work with RT-scaled magnitudes, H* = E* + 1
		self.Enthalpy = E_total + 1
		return E_total,E_terms

	def compute_entropy(self):
		'''Compute entropy term correction for an initialized GibbsHandler.
		Output:
		- S_total. Float, total entropy in terms of S/R
		- S_terms. List of floats, with translational/rotational/vibrational entropies in terms of S/R'''

		# Unpack temperature and pressure
		temp = self.temp
		pressure = self.Patm*101325 #atm to Pa
		#Translational entropy
		St_1 = 2*np.pi*self.mass_kg*spc.k*temp/spc.h**2
		St_2 = spc.k*temp/pressure*np.exp(5/2)
		S_transl = 3/2*np.log(St_1) + np.log(St_2)

		# Rotational entropy
		# Handling possible cases: linear, monoatomic & polyatomic

		Nrotors = len(self.rot_temps)
		if (Nrotors == 0):
			# Monoatomic: no rotation at all
			S_rot = 0.0
		elif (Nrotors == 1):
			# Linear molecule
			S_rot = np.log((np.exp(1)*temp)/(self.sigma*self.rot_temps[0]))
		else:
			#General case: polyatomic molecule
			prod_rot_temps = np.prod(self.rot_temps)
			S_rot = 0.5*np.log(np.pi*np.exp(3)*temp**3/(self.sigma**2*prod_rot_temps))

		# Vibrational entropy: use RRHO or Grimme depending on S_corr_type value
		print_now = (self.PrintLevel == 2)
		if (self.S_corr_type == "Grimme"):
			S_vibr = Svibr_Grimme(self.freqlist,temp,self.program_name,f_cutoff=self.FreqCutoff,
								  no_interp=self.CancelInterpolateRRHO,print_now=print_now)
		else:
			S_vibr = Svibr_RRHO(self.freqlist,temp,self.program_name,print_now)
			
		# Global entropy: combine and print. Electronic and nuclear degeneracies are directly omitted.
		# Given as S/R -> adimensional
		S_total = S_transl + S_rot + S_vibr
		self.Entropy = S_total
		S_terms = [S_transl,S_rot,S_vibr]
		self.EntropyComponents = S_terms
		return S_total,S_terms

	def compute_Gibbs_energy(self):
		'''Compute entropy term correction for an initialized GibbsHandler.
		Output:
		- G. Float, Gibbs free energy in terms of G/RT
		'''
		
		# E is E/RT and S is S/R: to get units right on output we must enter R at this point
		# G = E + RT - TS
		# G/RT = E/RT + 1 - S/R
		G = self.TotalEnergy + 1 - self.Entropy
		self.FreeEnergy = G
		return G

	def compute_thermodynamics(self,new_temp=None,out_unit="hartree"):
		'''Compute thermal energy, entropy and Gibbs energy for an initialized GibbsHandlerObj
		Simple wrapper for individual functions, also allowing for easy temperature modification
		'''
		if (new_temp):
			self.temp = new_temp
		Etotal,Ethermal_terms = self.compute_thermal_energy()
		Stotal,Sterms = self.compute_entropy()
		G = self.compute_Gibbs_energy()
		# Handle output: default to a.u., allow also to get kcal/mol or unitless G/RT
		G_values = {None:G,
					'hartree':G*R_hartree*self.temp,
					'kcal':G*R_kcal*self.temp}
		G_output = G_values.get(out_unit)
		return G_output

	def print_values(self):
		e_output = []
		s_output = []
		g_output = []
		temp = self.temp
		if (self.PrintLevel >= 1):
			e_output.append("E_el = \t %.6f a.u." % (self.Eelec*R_hartree*temp))
			g_output.append("G = \t %.6f \t a.u." % (self.FreeEnergy*R_hartree*temp))
			
		if (self.PrintLevel >= 2):
			e_output_add = ["E_el = \t %.6f a.u." % (self.Eelec*R_hartree*temp),
							"ET_corr = \t %.6f a.u." % (self.ThermalEnergy*R_hartree*temp),
							"E_thermal = \t %.6f a.u." % (self.TotalEnergy*R_hartree*temp),
							"H = \t\t %.6f a.u." % (self.Enthalpy*R_hartree*temp)]
			e_output += e_output_add
			s_output_add = ["S = \t %.4f cal mol-1 K-1" % (self.Entropy*R_kcal*1000),
							"TS = \t %.6f \t a.u." % (temp*self.Entropy*R_hartree)]
			s_output += s_output_add
			conditions = "Values at %.2f K and %.2f atm" % (self.temp,self.Patm)
			g_output.append(conditions)
			
		if (self.PrintLevel >= 3):
			e_terms = ["E_%s = %.6f a.u." % (term_name,Eterm*R_hartree*temp) for term_name,Eterm
					   in zip(["tr","rot","vib"],self.ThermalComponents)]
			e_output += e_terms
			s_terms = ["S_%s = %.4f cal mol-1 K-1" % (term_name,Sterm*R_kcal*1000) for term_name,Sterm
					   in zip(["tr","rot","vib"],self.EntropyComponents)]
			s_output += s_terms

		output_block = "\n".join(e_output + s_output + g_output)
		print(output_block)
		return output_block

	def print_values_lin(self,term_list=["E_el","E_thermal","H","TS","G"]):
		temp = self.temp
		RT_au = R_hartree * temp
		# here we may also have all terms and then modify things
		out_dict = {"E_el":self.Eelec*RT_au,"E_thermal":self.TotalEnergy*RT_au,"H":self.Enthalpy*RT_au,
					"TS":self.Entropy*RT_au,"G":self.FreeEnergy*RT_au}
		vals = [out_dict[term] for term in term_list]
		fmt_str = ",".join(["%.6f"] * len(vals))
		print(fmt_str % tuple(vals))
		return vals

	def full_init(self):
		self.cmd_IO()
		self.default_parser_reading()
		self.compute_thermodynamics()
		return self
	
def main():
	#Default execution from parser workflow
	Ghandle = GibbsHandler().full_init()
	Ghandle.print_values()
	return None

if __name__ == "__main__":
	main() 
