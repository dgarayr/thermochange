#!/bin/bash
#Diego Garay-Ruiz, Winter 2019
#Output energy, free energy, Martin-corrected G from a given output file from ADF or G09
#using the thermochange tool 

# optional args via getopts and shifting
while getopts "g" flag; do
  case "$flag" in
	g) corr_type="Grimme";;
	\?) corr_type="RRHO";;
  esac
done

shift $((OPTIND - 1))

#User input: filename, temp. in K, pressure in atm
filename=$1
temperature=$2
pressure=$3

# if nothing was set for T and P, adjust them for consistency
if [ -z $temperature  ] ; then
	temperature=298.15
fi

if [ -z $pressure  ] ; then
	pressure=1.0
fi


#Script location
script_route=${BASH_SOURCE[0]}
bash_script_name="formatted_energy_outputter.sh"
script_folder=$(echo "${script_route}" | sed "s|/${bash_script_name}||g" | sed "s|/formatters||g")
#Auto-choice of the program: ADF or G09
adf_flag=$(grep "Amsterdam Density Functional" $1)
if [ ! -z "$adf_flag" ] ; then
	#The ! -z operator checks if the variable is NOT EMPTY
	${script_folder}/formatters/adf_summary.sh ${filename} > temp_summary.temp
	E=$(grep "Bond Energy" temp_summary.temp | awk '{print $3}')
	G=$(grep "Gibbs" temp_summary.temp | awk '{print $4}')
	#Martin correction
	${script_folder}/thermochange_wrapper.sh ${filename} ${temperature} ${pressure} ${corr_type} > temp_summary.temp
	GM=$(grep "G =.*a.u" temp_summary.temp | awk '{print $3}')
	rm temp_summary.temp
	printf "%s\t%f\t%f\t%f\n" $filename $E $G $GM

else
	gaussian_flag=$(grep "Entering Gaussian System" $1)
	if [ ! -z "$gaussian_flag" ] ; then
		${script_folder}/formatters/g09_summary.sh ${filename} > temp_summary.temp
		E=$(grep "E(" temp_summary.temp | awk '{print $3}')
		G=$(grep "thermal Free Energies" temp_summary.temp | awk '{print $8}')	
		#Martin correction
		${script_folder}/thermochange_wrapper.sh ${filename} ${temperature} ${pressure} ${corr_type} > temp_summary.temp
		GM=$(grep "G =.*a.u" temp_summary.temp | awk '{print $3}')
		#rm temp_summary.temp
		printf "%s\t%f\t%f\t%f\n" $filename $E $G $GM
	else
		echo "No valid output file!"
	fi
fi

