#!/bin/bash
#Get ADF output file and retrieve thermochemical information
filename=$1
#fetch last occurrence of the energy summary
summary_begin_line_raw=$(grep -n 'Summary of energy terms' ${filename}  | tail -n1 | awk '{print $1}')
#trim the semicolon
summary_begin_line=$(echo $summary_begin_line_raw | sed "s/://g")
#skip two lines and get block of 6 (all thermodynamic data)
first_to_fetch=$(($summary_begin_line))
last_to_fetch=$(($summary_begin_line+8))
echo "Thermochemical information for ${filename}"
sed -n "$first_to_fetch,${last_to_fetch}p" $filename | cut -f1
