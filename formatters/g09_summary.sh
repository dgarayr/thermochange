#!/bin/bash
#Get G09 output file and retrieve thermochemical information
filename=$1
#start by getting last SCF energy
echo "SCF Energy"
e_line=$(grep "SCF Done" ${filename} | tail -n 1 | awk '{print $3,$4,$5,$6}')
# override for MP2 calculations, where SCF Done shows the HF energy, not the corrected value!
mp2_flag=$(grep "EUMP2" ${filename} | tail -n1)
if [ ! -z "${mp2_flag}" ] ; then
	  # extract the energy: overwrite
	  energy=$(echo $mp2_flag | awk '{gsub(/^.*EUMP2 =/,"");gsub(/D/,"E")} ; {printf "%20.9f\n",$0}')
	#echo "MP2 calculation found: overriding SCF energy"
	echo "E(MP2) = $energy"
else
	echo $e_line
fi

#Fetch the thermochemistry block: start by Zero-point correction.
summary_begin_line_raw=$(grep -n 'Zero-point correction=' ${filename}  | tail -n1 | awk '{print $1}')
#trim the semicolon
summary_begin_line=$(echo $summary_begin_line_raw | sed "s/://g")
#Start here and get the next 7 lines to have all info
first_to_fetch=$(($summary_begin_line))
last_to_fetch=$(($summary_begin_line+7))
echo "Thermochemical information for ${filename}"
#include the temperature and pressure of this calculation
grep "Temperature" $1
echo "===================================="
sed -n "$first_to_fetch,${last_to_fetch}p" $filename | cut -f1
