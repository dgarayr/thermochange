#!/bin/bash
#Diego Garay-Ruiz, Spring 2019. Bash wrapper for ADF & Gaussian parsers
#to extract information from ADF/Gaussian output and run thermochange.py

#Script location
script_route=${BASH_SOURCE[0]}
wrap_script_name="thermochange_wrapper"
script_folder_main=$(echo "${script_route}" | sed "s|/${wrap_script_name}.sh||g")
script_folder="${script_folder_main}/parsers"

#Check the program and call the corresponding parser
adf_flag=$(grep "Amsterdam Density Functional" $1)
ams_flag=$(grep "Amsterdam Modeling Suite" $1)


if [ ! -z "$adf_flag" ] ; then
	#The ! -z operator checks if the variable is NOT EMPTY
	echo "===> ADF output file <===="
	echo "=========================="
	${script_folder}/parser_adf.sh $1 $2 $3 $4 
elif [ ! -z "$ams_flag" ] ; then 
	echo "===> AMS output file <===="
	echo "=========================="
	${script_folder}/parser_ams.sh $1 $2 $3 $4 
else
	gaussian_flag=$(grep "Entering Gaussian System" $1)
	if [ ! -z "$gaussian_flag" ] ; then
		echo "===> Gaussian 09 output file <===="
		echo "=================================="
		${script_folder}/parser_g09.sh $1 $2 $3 $4 
	else
		echo "No valid output file!"
	fi
fi
