import numpy as np
import sys
from scipy.constants import h as h_joule
from scipy.constants import k as kb
from scipy.constants import c, physical_constants
from scipy.constants import R as R_SI
from scipy.constants import Avogadro as Navog
import scipy.constants

#====Definition of vibr. term calculation functions====
def Evibr_RRHO(freqlist,temp,program):
	'''
	Compute vibrational contribution to thermal energy.

	Input: 
	- freqlist. List of floats: frequencies in cm-1.
	- temp. Float, temperature in K.
	- program. String, stating the program: ADF or G09.
	Output:
	- E_vibr. Float, vibrational energy.
	'''
	E_vib=0.0
	#ADF ignores frequencies under 20 cm-1: take this into account.
	if (program_name=="ADF"):
		freqlimit=20
	elif (program_name=="G09"):
		freqlimit=0
		
	for freq in freqlist:
		if (freq < freqlimit):
			continue
		theta_v=h*freq/kb
		term1=theta_v/(2*temp)
		term2=(theta_v/temp)/(np.exp(theta_v/temp)-1)
		term=term1+term2
		E_vib=E_vib+term
		#ZPVE correction is already included
	return E_vib

def Svibr_RRHO(freqlist,temp,program):
	'''
	Compute vibrational entropy with RRHO traditional expression
	(rigid rotor/harmonic oscillator)
	
	Physical constants require scipy.constants and numpy
	Input: 
	- freqlist. List of floats: frequencies in cm-1.
	- temp. Float, temperature in K.
	- program. String, stating the program: ADF or G09.
	Output:
	- Svibr. Float, vibr. entropy from RRHO approx.	
	'''
	#Constant definition
	c=scipy.constants.c
	h_joule=scipy.constants.h
	h=h_joule*c*100		#to use wavenumbers in cm-1
	kb=scipy.constants.k
	#Calculation: consider the cutoff of small freqs. of different programs
	S_vibr=0
	skipped_freqs=0
	if (program_name=="ADF"):
		freqlimit=20
	elif (program_name=="G09"):
		freqlimit=0
	for freq in freqlist:
		if (freq < freqlimit):
			print("Neg. freq. %.2f cm-1" % freq)
			skipped_freqs=skipped_freqs+1
			continue
		theta_v=h*freq/kb
		term1=(theta_v/temp)/(np.exp(theta_v/temp)-1)
		term2=np.log(1-np.exp(-theta_v/temp))
		s_vibr_term=term1-term2
		S_vibr=S_vibr+s_vibr_term
	print("Skipped",skipped_freqs,"small or negative frequencies")
	return S_vibr

def Svibr_Grimme(freqlist,temp,program):
	'''
	Compute vibrational entropy with pseudorrotational approach for small
	frequencies: below the cutoff, normal modes are also treated as rotations
	and the entropy contribution is interpolated between rot. and vibr. expressions
	
	Physical constants require scipy.constants and numpy
	Input: 
	- freqlist. List of floats: frequencies in cm-1.
	- temp. Float, temperature in K.
	- program. String, stating the program: ADF or G09.
	'''
	#Constant definition
	c=scipy.constants.c
	h_joule=scipy.constants.h
	h=h_joule*c*100		#to use wavenumbers in cm-1
	kb=scipy.constants.k
	#Calculation
	S_vibr=0
	skipped_freqs=0
	f_cutoff=100		#in cm-1, from Grimme Chem. Eur. J. 2012
	Bav=10E-44			#in kg·m2, from Grimme Chem. Eur. J. 2012
	if (program=="ADF"):
		freqlimit=20
	elif (program=="G09"):
		freqlimit=0
	for freq in freqlist:
		if (freq < freqlimit):
			#Skip small/negative frequencies (Cutoff depends on specific program)
			print("Neg. freq. %.2f cm-1" % freq)
			skipped_freqs=skipped_freqs+1
			continue
		#Both RRHO and pseudorrotational contributions must be computed, then weighted

		#Pseudorrotational approach (Grimme Chem. Eur. J. 2012):
		freq_hz=freq*100*c
		m_inertia=h_joule/(8*np.pi**2*freq_hz)
		m_inertia_eff=m_inertia*Bav/(m_inertia+Bav)
		logterm=8*np.pi**3*m_inertia_eff*kb*temp/h_joule**2
		s_vibr_pseudorrot=0.5*(1+np.log(logterm))

		#RRHO approximation
		theta_v=h*freq/kb
		term1=(theta_v/temp)/(np.exp(theta_v/temp)-1)
		term2=np.log(1-np.exp(-theta_v/temp))
		s_vibr_rrho=term1-term2

		#Weight according to the quotient against the cutoff freq. 100 cm-1
		weight=1/(1+(f_cutoff/freq)**2)
		s_vibr_term=weight*s_vibr_rrho+(1-weight)*s_vibr_pseudorrot
		S_vibr=S_vibr+s_vibr_term
	print("Skipped",skipped_freqs,"small or negative frequencies")
	return S_vibr

#====Input management: check that all variables are provided====
program_name=sys.argv[1]
if len(sys.argv) < 4:
	print("Enter T/K and P/atm for the calculation")
	print("Using defaults: 298.15 K, 1 atm")
	temp=298.15
	Patm=1.0
	S_corr_type="RRHO"
else:
	temp=float(sys.argv[2])
	Patm=float(sys.argv[3])
	sigma=int(sys.argv[4])
	print("Symmetry number=",sigma)
	S_corr_type=str(sys.argv[5])

#====Unit conversions====
pressure=Patm*101325 	#atm to Pascal
h=h_joule*c*100 		#to multiply by cm-1 wavenumbers
amu_to_kg=physical_constants['atomic mass constant'][0]
bohr_to_m=physical_constants['atomic unit of length'][0]
hartree_to_J=physical_constants['atomic unit of energy'][0]
eV_to_J=physical_constants['atomic unit of charge'][0]
R_kcal=R_SI/(4.184*1000) #in kcal/(mol K)
R_hartree=R_SI/(hartree_to_J*Navog)
kcalmol_to_hartree=4184/(hartree_to_J*Navog)

#====File processing: read info from parsing script========#
freqlist=[]
with open ('freq_info') as filefreqs:
	lines=filefreqs.read()
	for item in lines.split():
		#Adjust precision to match with recalc. freqs for ADF
		num_item=float(item)
		freqlist.append(round(num_item,3))

#ADF-only: processing freqs. obtained after recalculation (SCANFREQ)
if (program_name=="ADF"):
	recalclist=[]
	with open ('recalc_freqs') as filerecalc:
		lines=filerecalc.read()
		for item in lines.split():
			recalclist.append(float(item))
	num_items=len(recalclist)
	freq_pairs=[]
	for i in range(int(num_items/2)):
		freq_pairs.append((recalclist[2*i],recalclist[2*i+1]))
	original_frequencies=freqlist
	counter=0
	for pair in freq_pairs:
		if pair[0] in freqlist:
			index_match=freqlist.index(pair[0])
			print("Substituting freq.",pair[0],"by",pair[1])
			freqlist[index_match]=pair[1]
			counter=counter+1
	if (counter != len(freq_pairs)):
		print("Some modified frequencies were not taken into account")
	freqlist=sorted(freqlist)
else:
	#For G09, just sort the list
	freqlist=sorted(freqlist)

#Energy: convert from a.u. (input) to Joule
with open('energy.txt') as fileenerg:
	energy_au=float(fileenerg.read())
	energy=energy_au*hartree_to_J

#Rotational magnitudes: G09 provides rot. temps and ADF, moments of inertia
if (program_name=="G09"):
	rot_temps=[] #in K
	with open('rot_temps.txt') as fileinert:
		lines=fileinert.read()
		for item in lines.split():
			rot_temps.append(float(item))
elif (program_name=="ADF"):
	moments_inertia=[] #in amu bohr²
	with open('moments_inertia.txt') as fileinert:
		lines=fileinert.read()
		for item in lines.split():
			moments_inertia.append(float(item))
#Mass: in amu
with open('mass.txt') as filemass:
	mass=float(filemass.read())
	mass_kg=mass*amu_to_kg

#====Energy (E/NkT) calculation====
#Electronic energy (for ADF, bonding energy)
Ebond=energy
#Translational and rotational energies: consider 3/2 RT expression
# (equipartition theorem: 1/2 RT for every degree of freedom)
E_transl=1.5
E_rot=1.5

#This does not hold for linear molecules, lacking one rot. deg. of freedom
#so E_rot = 2 * (1/2)RT = RT
#Linearity is determined afterwards, during entropy evaluation (according to the num.
# of rot. temperatures. E_rot will be corrected there

#For monoatomics there is no rotation: this is caught with a flag for the symmetry number
if (sigma==999):
	E_rot=0.0

#Vibrational energy: sum through all terms
E_vib=Evibr_RRHO(freqlist,temp,program_name)

#Sum & output printing:
thermal_corr=E_transl+E_rot+E_vib
print("-------------")
print("Thermal corr.= %.4f kcal/mol" % (thermal_corr*R_kcal*temp))
print("Thermal corr.= %.6f a.u." %(thermal_corr*R_kcal*temp*kcalmol_to_hartree))
print("Contributions to thermal correction (kcal/mol): translation, rotation and vibration")
print("%.4f    %.4f    %.4f" % (E_transl*R_kcal*temp,E_rot*R_kcal*temp,E_vib*R_kcal*temp))
E_overall=thermal_corr+Ebond/(kb*temp)
#in terms of E/kT
print("E(SCF)= \t %.4f kcal/mol" % (energy/(kb*temp)*R_kcal*temp))
print("E(SCF)= \t %.6f a.u." % (energy/hartree_to_J))
print("E_thermal= \t %.6f a.u." %((energy/hartree_to_J+thermal_corr*R_kcal*temp*kcalmol_to_hartree)))
print("-------------")

#====Entropy calculation=====#
#Translational entropy
St_1=2*np.pi*mass_kg*kb*temp/h_joule**2
St_2=kb*temp/pressure*np.exp(5/2)
S_transl=3/2*np.log(St_1)+np.log(St_2)

#Rotational entropy
#For G09 we already have rotational temperatures
#ADF requires to compute them from moments of inertia
if (program_name=="ADF"):
	rot_temps=[]
	for Ivalue in moments_inertia:
		#be careful with the linear case which includes null entries
		if (Ivalue == 0.0):
			unique_Ivals=list(set(moments_inertia))
			unique_Ivals.remove(0.0)
			Ivalue_SI=unique_Ivals[0]*amu_to_kg*(bohr_to_m)**2
			theta=h_joule**2/(8*(np.pi)**2*kb*Ivalue_SI)
			rot_temps.append(theta)
			moments_inertia=unique_Ivals
			break
		#Transform to kg m² and get rot. temperatures in K
		Ivalue_SI=Ivalue*amu_to_kg*(bohr_to_m)**2
		theta=h_joule**2/(8*(np.pi)**2*kb*Ivalue_SI)
		rot_temps.append(theta)

#Handling possible cases: linear, monoatomic & polyatomic
if len(rot_temps)==1:
	#Linear molecule
	print("="*21)
	print("Linear molecule identified")
	S_rot=np.log((np.exp(1)*temp)/(sigma*rot_temps[0]))
	#Correction of thermal contribution for linear molecules: was considered as 3/2 RT but shall be RT
	print("Correcting energy: used 3/2 RT for rot. contribution, but for lin. mol. is RT")
	thermal_corr=thermal_corr-0.5
	print("Thermal*=  ",(thermal_corr-0.5)*R_kcal*temp)
	print("Ecorr*=	",(energy/(hartree_to_J*kcalmol_to_hartree)+thermal_corr*R_kcal*temp)*kcalmol_to_hartree)
	print("="*21)
elif len(rot_temps)==0:
	#Monoatomic
	S_rot=0.
else:
	#General case
	S_rot=0.5*np.log(np.pi*np.exp(3)*temp**3/(sigma**2*rot_temps[0]*rot_temps[1]*rot_temps[2]))

#Vibrational entropy
#Use RRHO or Grimme depending on parser result
print("%s term for vibrational entropy" % S_corr_type)
if (S_corr_type=="Grimme"):
	S_vibr=Svibr_Grimme(freqlist,temp,program_name)
else:
	S_vibr=Svibr_RRHO(freqlist,temp,program_name)
#Global entropy: combine and print

#Electronic and nuclear degeneracies are omitted
#If needed in any case, might be included afterwards
S_total=S_transl+S_rot+S_vibr

#Conversion factor to get in cal/(mol K) to compare with G09 results
print("S= \t %.4f kcal/mol" % (S_total*R_kcal*1000))
print("Contributions to entropy (in cal mol-1 K-1): translation, rotation, vibration")
print("%.4f    %.4f    %.4f" % (S_transl*R_kcal*1000,S_rot*R_kcal*1000,S_vibr*R_kcal*1000))
print("-------------")

#====Obtaining Gibbs free energy
#E is E/RT and S is S/R: to get units right on output we must consider R now
#G = E + RT - TS

#G_old was original implementation, with more numeric error due to many unit changes
#G_old=E_overall*R_kcal*temp+R_kcal*temp-temp*S_total*R_kcal
#Current expression is more robust

G=energy/(hartree_to_J*kcalmol_to_hartree)+thermal_corr*R_kcal*temp+R_kcal*temp-temp*S_total*R_kcal

#Printing output
print("PV= \t %.4f \t kcal/mol" % (R_kcal*temp))
print("TS= \t %.4f \t kcal/mol" % (temp*S_total*R_kcal))
print("G= \t %.4f \t kcal/mol" % G)
print("G=	%.6f \t a.u." % (G*kcalmol_to_hartree))
print("Values at ",temp,"K and ",Patm," atm")
